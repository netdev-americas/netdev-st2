"""This module provides a Requests interface for NetDev Tenant Demo."""
from st2actions.runners.pythonrunner import Action
import requests
import logging

from urlparse import urlparse

LOG_FILENAME = 'run_cmd.out'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)


class RunCmd(Action):
    """Runcmd Class."""

    def snake_case_to_kebab_case(self, s):
        """Convert snake case to kebab_case."""
        return s.replace("_", "-")

    def parse_device_info(self, device_info_string):
        """Parse connect string into individual parts."""
        uri = urlparse(device_info_string)
        if (uri):
            result = (True, {"host": uri.hostname, "port": uri.port, "username": uri.username,
                              "password": uri.password})
        else:
            result = (False, None)
        return result

    def massage_value(self, value):
        value_type = type(value)
        massaged_value = None
        
        if value_type == bool:
            massaged_value = str(value).lower()
        else:
            massaged_value = value

        return massaged_value


    def run(self, **kwargs):
        """Runner Method."""
        # copy, because Python design
        params = {}
        params.update(kwargs)

        if "device_info_string" in params:
            device_info_string = params.get("device_info_string")
            (success, device_params) = \
                self.parse_device_info(str(device_info_string))
            if success:
                params.update(device_params)

        payload_args = params["payload_args"]
        payload = {}
        for a in payload_args:
            key = self.snake_case_to_kebab_case(a)
            val = self.massage_value(params[a])
            payload[key] = val

        headers = {"Accept": "application/json"}
        url = params.get("url")
        http_method = params.get("http_method")
        http_action = None
        if http_method == "GET":
            http_action = lambda x, y: requests.get(x, params=y, headers=headers)  # noqa: E731
        elif http_method == "POST":
            http_action = lambda x, y: requests.post(x, params=y, headers=headers)  # noqa: E731
        else:
            return (False, "Error, http_method supplied isn't GET or POST")

        logging.debug("DEBUG: url: {} payload: {}".format(url, payload))
        r = http_action(url, payload)
        return (r.status_code == 200, r.text)
